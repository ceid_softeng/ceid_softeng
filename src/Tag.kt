data class Tag(
      private val tag_id:String,
      private val plc_name:String,
      private val plc_altitude:ULong,
      private val pac_longitude: ULong,
      private val date_year:Int,
      private val date_month:Int,
      private val date_day:Int
)