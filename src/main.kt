fun main(args: Array<String>){
    var usr = User("Kosts")

    println("Hello world!\nDo you want to PetMe ${usr.getUserName()}?")

    usr.setUserName("Kostas")
    usr.setPassword("thisIsMyNewPasswd")

    println(".. oh, i am sorry. Your username is \"${usr.getUserName()}\" \nand your password \"${usr.getPassword()}\"")

    usr.prof.setCompetitionTimeStamp()
    println("Your compTimestamp \"${usr.prof.getCompetitionTimestampMS()}\"")
}
